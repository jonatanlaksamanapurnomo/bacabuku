@extends('layouts.admin')

@section('content')



    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="sidebar navbar-nav">
            <li class="nav-item active">
                <a class="nav-link" href="#">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span>
                </a>
            </li>

        </ul>

        <div id="content-wrapper">

            <div class="container-fluid">

                <!-- Breadcrumbs-->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="#">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active">Overview</li>
                </ol>

                <!-- Icon Cards-->
                <div class="row">

                    <div class="col-xl-6 col-sm-6 mb-3">
                        <div class="card text-white bg-danger o-hidden h-100">
                            <div class="card-body">
                                <div class="card-body-icon">
                                    <i class="fas fa-fw fa-life-ring"></i>
                                </div>
                                <div class="mr-5">{{count($buku)}} Books</div>
                            </div>

                        </div>
                    </div>

                    <div class="col-xl-6 col-sm-6 mb-3">
                        <div class="card text-white bg-success o-hidden h-100">
                            <div class="card-body">
                                <div class="card-body-icon">
                                    <i class="fas fa-fw fa-shopping-cart"></i>
                                </div>
                                <div class="mr-5">{{count($oderitems)}} Orders</div>

                            </div>
                            <a class="card-footer text-white clearfix small z-1" href="{{route('admin.order')}}">
                                <span class="float-left">View Details</span>
                                <span class="float-right">
                    <i class="fas fa-angle-right"></i>
                  </span>
                            </a>
                        </div>
                    </div>

                </div>



                <!-- DataTables Example -->
                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fas fa-table"></i>
                        Book Table</div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Pengarang</th>
                                    <th>Penerbit</th>
                                    <th>Harga</th>
                                    <th>Action</th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach($buku as $item)
                                    <tr>
                                        <td>{{$item->title}}</td>
                                        <td>{{$item->pengarang}}</td>
                                        <td>{{$item->penerbit}}</td>
                                        <td>{{$item->price}}</td>
                                        <td>  <form method="post" action="{{URL::to('home/'.$item->id.'/delete')}}">
                                                {{csrf_field()}}
                                                {{method_field('delete')}}
                                                <input  type="submit" value="Delete" class=" btn btn-danger">

                                            </form>
                                            <a href="{{URL::to('home/' . $item->id . '/edit')}}" class="btn btn-primary">Edit</a>

                                        </td>

                                    </tr>
                                @endforeach


                                </tbody>
                            </table>
                            {{$buku->links()}}
                        </div>
                    </div>
                   <a href="{{route('add.book')}}" class="btn btn-outline-primary">Add Book</a>
                </div>

            </div>

@endsection
