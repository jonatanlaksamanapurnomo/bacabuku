@extends('layouts.app')
@section('content')
<section>

    <div class="container">
        <div class="jumbotron">
            <table>
                <tr><td>Order No : {{count($order)}}</td></tr>
                <tr><td>Judul Buku: {{$buku->title}}</td></tr>
                <tr><td>Pengarang Buku: {{$buku->pengarang}}</td></tr>

            </table>

        </div>
        <form method="post" action="{{route('updateStore.order' , $buku)}}" enctype="multipart/form-data" >
            {{csrf_field()}}
            {{method_field("PATCH")}}
            <input type="hidden" value="{{$buku->id}}" name="idBuku">
            <input type="hidden" value="{{$user->id}}" name="idUser">
            <input type="hidden" value="{{$buku->price}}" name="harga">
            <input type="hidden" value="{{$user->name}}" name="namaPenerima">
            <input type="hidden" value="{{$user->email}}" name="emailPenerima">

            <div class="form-group">
                <label for="penerbit">Nama Penerima</label>
                <input type="text" name="" disabled class="form-control" value="{{$user->name}}" placeholder="">
            </div>
            <div class="form-group">
                <label for="penerbit">Email Penerima</label>
                <input type="text" name="emailPenerima" disabled class="form-control" value="{{$user->email}}" placeholder="">
            </div>
            <div class="form-group">
                <label for="synopsis">Alamat</label>
                <textarea class="form-control" style="min-width: 25%;"  name="alamat" required> </textarea>
            </div>
            <div class="form-group">
                <label for="price">Telphone Number</label>
                <input type="number" class="form-control" min="0" value=""  required name="phoneNumber">
            </div>
            <div class="form-group">
                <label for="price">Harga Buku</label>
                <input type="number" class="form-control" min="0" value="{{$buku->price}}" disabled  name="price">
            </div>

            <div class="form-group">

                <input type="submit" value="Order Now"  class="btn btn-success">
            </div>
        </form>
</section>



@endsection

