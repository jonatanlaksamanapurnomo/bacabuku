@extends('layouts.app')
@section('content')

    <!-- Page Content -->
    <div class="container">

        <!-- Jumbotron Header -->
        <header class="jumbotron my-4">
            <h1 class="display-3">Something to say</h1>
            <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa, ipsam, eligendi, in quo sunt possimus non incidunt odit vero aliquid similique quaerat nam nobis illo aspernatur vitae fugiat numquam repellat.</p>
            <a href="#" class="btn btn-primary btn-lg">Hot Deals!</a>
        </header>

        <!-- Page Features -->

        <div class="row text-center">
            @foreach($buku as $item)
                <div class="col-lg-3 col-md-6 mb-4">
                    <div class="card">
                        <img class="card-img-top" src="{{asset('storage/Images/'.$item->image )}}" alt="">
                        <div class="card-body">
                            <h4 class="card-title">{{$item->title}}</h4>
                            <p class="card-text">{{$item->synopsis}}</p>
                        </div>
                        <div class="card-footer">
                            <a href="{{URL::to('orders/' . $item->id )}}" class="btn btn-primary">Rp {{$item->price}}</a>

                        </div>
                    </div>
                </div>


            @endforeach





        </div>
    {{$buku->links()}}
        <!-- /.row -->

    </div>

@endsection
