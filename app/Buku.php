<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buku extends Model
{
    //
  protected  $fillable = ['title' , 'penerbit' , 'image' , 'pengarang' , 'synopsis','price'];
}
